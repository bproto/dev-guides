===================
Building with CMake
===================

.. note::
    This guide has been based on LLVM's `Building LLVM with CMake <https://llvm.org/docs/CMake.html>`__.

.. hint::
    This page is targeted toward new users of the CMake build system.
    If you're looking for information about modifying the files used by CMake you may want to see the :doc:`primer` page.
    It has a basic overview of the CMake language.



Introduction
============



`CMake <http://www.cmake.org/>`__ is a cross-platform build-generator tool.
CMake itself does not build the project, instead, it generates the files required by your build tool (GNU make, Visual Studio, etc).


If you have never worked with CMake before, start with the :ref:`cmake-basic-usage` section.
If you are already familiar with the basics, skip right to the :ref:`cmake-quick-start`.
The :ref:`cmake-variables` section is a reference for customizing your build.


.. _cmake-basic-usage:

Basic CMake usage
=================

This section explains fundamental aspects of CMake that you may need in your day-to-day usage.

CMake comes with extensive documentation in the form of html files, and as online help accessible via the ``cmake`` executable itself.
Execute ``cmake --help`` for further help options.

CMake allows you to specify a build tool (e.g., GNU make, Visual Studio, or Xcode).
If not specified on the command line, CMake tries to guess which build tool to use based on your environment.
Once it has identified your build tool, CMake uses the corresponding *Generator* to create files for your build tool.
You can explicitly specify the generator with the command line option ``-G "Name of the generator"``.
To see a list of the available generators on your system, execute

.. prompt:: bash

    cmake --help

This will list the generator names at the end of the help text.

Generators' names are case-sensitive and may contain spaces.
For this reason, you should put the exact name from the list given by ``cmake --help`` output using quotation marks.
For example, to generate project files specifically for Visual Studio 12, you need to execute the next command:

.. prompt:: bash

    cmake -G "Visual Studio 12" path/to/source/root

For a given development platform there can be more than one fitting generator.
By default, CMake chooses the most specific generator supported by your development environment.
If you want an alternative generator, you must tell this to CMake with the ``-G`` option.


.. _cmake-quick-start:

Quick start
===========

Here the command-line, non-interactive CMake interface is used.

#. `Download <http://www.cmake.org/cmake/resources/software.html>`__ and install CMake.

   .. warning:: Version 3.15 is the minimum requirement for any project under the `BPROTO group <https://gitlab.com/bproto>`__.

#. Open shell.
   Your development tools must be reachable from this shell through the ``PATH`` environment variable.

#. Configure CMake project:

   .. warning:: Building a CMake project in the source directory is usually not supported and must be avoided.

   #. Classic way

      Create a build directory.
      ``cd`` to this directory:

      .. prompt:: bash

        mkdir mybuilddir
        cd mybuilddir
        cmake path/to/source/root

   #. "Lazy" way

      CMake will automatically create the `path/to/build/root` folder if it does not exist.

      .. prompt:: bash

        cmake -S path/to/source/root -B path/to/build/root

   CMake will detect your development environment, perform a series of tests, and generate the files required for building a CMake project.
   CMake will use default values for all build parameters.
   See the :ref:`cmake-variables` section for a list of build parameters that you can modify.

   This can fail if CMake can't detect your toolset, or if CMake sanity tests have failed.
   In this case, make sure that the toolset that you intend to use is the only one reachable from the shell, and that the shell itself is the correct one for your development environment.
   CMake will refuse to build MinGW makefiles if you have a POSIX shell reachable through the ``PATH`` environment variable, for instance.
   You can force CMake to use a given build tool; for instructions, see the :ref:`cmake-basic-usage` section, below.

#. After CMake has finished running, proceed to use IDE project files, or start the build from the build directory:

   .. prompt:: bash

      cmake --build .

   The ``--build`` option tells ``cmake`` to invoke the underlying build tool (``make``, ``ninja``, ``xcodebuild``, ``msbuild``, etc.)

   The underlying build tool can be invoked directly, of course, but the ``--build`` option is portable.

#. After the project has finished building, install it from the build directory:

   .. prompt:: bash

      cmake --build . --target install

   The ``--target`` option with ``install`` parameter in addition to the ``--build`` option tells ``cmake`` to build the ``install`` target.

   It is possible to set a different install prefix at installation time by invoking the ``cmake_install.cmake`` script generated in the build directory:

   .. prompt:: bash

      cmake -DCMAKE_INSTALL_PREFIX=/tmp/install/dir -P cmake_install.cmake


.. _cmake-variables:

Standard options and variables
==============================

Variables customize how the build will be generated.
Options are boolean variables, with possible values ``ON`` / ``OFF``.
Options and variables are defined on the CMake command line like this:

.. code-block:: console

  $ cmake -DVARIABLE=value path/to/source
  # or
  $ cmake -S path/to/source -B path/to/build -DVARIABLE=value

You can set a variable after the initial CMake invocation to change its value.
You can also undefine a variable:

.. code-block:: console

  $ cmake -UVARIABLE path/to/source
  # or
  $ cmake -S path/to/source -B path/to/build -UVARIABLE

Variables are stored in the CMake cache.
This is a file named ``CMakeCache.txt`` that is stored at the root of your build directory generated by ``cmake``.
Editing it yourself is not recommended.

Variables are listed in the CMake cache and later in this document with the variable name and type separated by a colon.
You can also specify the variable and type on the CMake command line:

.. code-block:: console

  $ cmake -DVARIABLE:TYPE=value path/to/source
  # or
  $ cmake -S path/to/source -B path/to/build -DVARIABLE:TYPE=value

.. _cmake-frequent-variables:

Frequently-used CMake variables
-------------------------------

Here are some of the CMake variables that are used often, along with a brief explanation.
For the full documentation, consult the `CMake manual <https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html>`__, or execute ``cmake --help-variable VARIABLE_NAME``.

**CMAKE_BUILD_TYPE**:STRING
  Sets the build type for ``make``-based generators.
  Possible values are ``Release``, ``Debug``, ``RelWithDebInfo`` and ``MinSizeRel``.
  If you are using an IDE such as Visual Studio, you should use the IDE settings to set the build type.
  Be aware that ``Release`` and ``RelWithDebInfo`` use different optimization levels on most platforms.

**CMAKE_INSTALL_PREFIX**:PATH
  Path where CMake project will be installed when the "install" target is built.

**CMAKE_{C,CXX}_FLAGS**:STRING
  Extra flags to use when compiling C and C++ source files respectively.

  .. warning:: This variable must be used only in exceptional cases.

**CMAKE_{C,CXX}_COMPILER**:STRING
  Specify the C and C++ compilers to use.
  If you have multiple compilers installed, CMake might not set the one you wish to use as a default.

Executing the Tests
===================

Testing of the projects under the `BPROTO group <https://gitlab.com/bproto>`__ is performed using ``ctest`` command.
The ``ctest`` executable is the CMake test driver program.
CMake-generated build trees created for projects that use the ``enable_testing()`` and ``add_test()`` commands have testing support.
This program runs the tests and reports results.

For more information on ``ctest`` command visit `ctest manual <https://cmake.org/cmake/help/latest/manual/ctest.1.html>`__.


Cross-compiling
===============

CMake uses a toolchain of utilities to compile, link libraries and create archives, and other tasks to drive the build.

You can find general instructions on how to cross-compile with CMake on `this page <https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html>`__.
It goes into detailed explanations and may seem complicated, but there are several examples including toolchain files.

