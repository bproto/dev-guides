############
CMake Guides
############

How `CMake <https://cmake.org/>`__ is used by project.

.. toctree::
   :maxdepth: 1
   :hidden:

   quick_start.rst
   primer.rst

- :doc:`quick_start`
- :doc:`primer`