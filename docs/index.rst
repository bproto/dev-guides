###############
Developer Guide
###############

.. toctree::
   :caption: Guides
   :maxdepth: 1
   :hidden:

   cmake/index.rst
   cpp/index.rst
   docker/index.rst
   editors/index.rst
   restructuredtext/index.rst
   shell/index.rst

   git/index.rst
