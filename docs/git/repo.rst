##########
Repository
##########

.. caution::
    This guide has been based on `LSST <https://developer.lsst.io/index.html>`__'s Git guide.
    Please respect the creators and contributor of the original guide as they have done an external work.

This page describes our basic workflows with our repositories that are hosted on `GitLab.com <https://github.com>`__.

In addition, we suggest some *best practices* for maximizing the usefulness of our Git development history:

- :doc:`best_practices`.
- :doc:`commit_message`.

Git & GitHub Setup
==================

You need to install Git version 1.8.2, or later to work with our data repositories.

Follow these steps to configure your Git environment:

1. :ref:`Set Git and GitLab to use your employer-hosted email address <git-setup-employer-email>`.
2. :ref:`Set Git to use 'plain' pushes <git-setup-plain-pushes>`.

*See also:* :doc:`/git/setup`.

GitLab Groups
=============

.. _bproto: https://gitlab.com/bproto

Right now Git repositories are available from one GitLab group: `bproto`_.
Project source code is proprietary available and protected by by trade secret and copyright law.

You should already be a member of the `bproto`_ GitLab group.
If you cannot create repositories or push to repositories there, ask your team leader or project manager to add you to this group.

bproto GitLab group
-------------------

The `bproto`_ GitLab group is for source-code and documentation repositories.

.. _git-branching:

Git Branching Policy
====================

Rather than forking GitLab repositories, developers use a *shared repository model* by cloning repositories in the `bproto`_ GitLab group.
Since the GitHub ``origin`` remotes are shared, it is essential that developers adhere to the following naming conventions for branches.

*See also:*

* `Gitflow Workflow <https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow>`__.
* `A successful Git branching model <https://nvie.com/posts/a-successful-git-branching-model/>`__.

.. _git-branch-release-history:

The master branch
-----------------

We consider ``master`` to be the main branch where the source code of ``HEAD`` always reflects a *production-ready* state, and therefore stores the official release history.

When the source code in the ``develop`` branch reaches a stable point and is ready to be released, all of the changes are merged back into ``master`` and then tagged with a release number.

Therefore, each time when changes are merged back into master, this is a new production release by definition.
We tend to be very strict at this, as we use a Git hook script to automatically build and roll-out our new software version every time there was a commit on master.

.. warning::

    The Git history of ``master`` **must never be re-written** with force pushes.

.. _git-branch-integration:

The develop branch
------------------

We consider ``develop`` be the main branch where the source code of ``HEAD`` always reflects a state with the latest delivered development changes for the next release.
Some would call this the *"integration branch"*.

The ``develop`` branch should always be stable and deployable, therefore development is not done directly on the ``develop`` branch, but instead on *ticket branches*.

.. warning::

    The Git history of ``develop`` **must never be re-written** with force pushes.

.. _git-branch-user:

Feature branches
----------------

You can do experimental, proof-of-concept work in 'feature branches.'

These branches are named

.. code-block:: text

   feature/{{topic}}

Feature branches can be pushed to GitLab to enable collaboration and communication.
Before offering unsolicited code review on your colleagues' ``feature`` branches, remember that the work is intended to be an early prototype.

Developers can feel free to rebase and force push work to their personal ``feature`` branches.

A user branch *cannot* be merged into ``develop``; it must be converted into a *ticket branch* first.
The only exception is for documentation edits and additions.

.. _git-branch-ticket:

Ticket branches
---------------

Ticket branches are associated with a JIRA ticket.
Only ticket branches can be merged into ``develop``.
(In other words, developing on a ticket branch is the only way to record earned value for code development.)

If the JIRA ticket is named ``BE-NNNN``, then the ticket branch will be named

.. code-block:: text

   tickets/BE-NNNN

A ticket branch can be made by branching off an existing ``feature`` branch.

When code on a ticket branch is ready for review and merging, follow the :ref:`code review process documentation <workflow-code-review>`.
