==========================
Commit Message Conventions
==========================

Generally you should write your commit messages in an editor, not at the prompt.
Reserve the ``git commit -m "message"`` pattern for 'work in progress' commits that will be rebased before code review.

We follow standard conventions for Git commit messages, which consist of a short summary line followed by body of discussion.

*See also:*

* `Tim Pope wrote about commit message formatting <http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html>`__.
* `How to Write a Git Commit Message <https://cbea.ms/git-commit/>`__.

Writing commit summary lines
----------------------------

**Messages start with a single-line summary of 50 characters or less**.

Consider 50 characters as a hard limit (but limit of 72 characters still can be considered); your summary will be truncated in the GitLab UI otherwise.

**Capitalize the subject line**.

This is as simple as it sounds.
Begin all subject lines with a capital letter.

For example:

    Accelerate to 88 miles per hour

Instead of:

    accelerate to 88 miles per hour

**Do not end the subject line with a period**.

Trailing punctuation is unnecessary in subject lines.
Besides, space is precious when you're trying to keep them to 50 chars or less.

Example:

    Open the pod bay doors

Instead of:

    Open the pod bay doors.

**Use the imperative mood in the subject line**.

Write the message in the **imperative** tense, not the past tense.
A properly formed Git commit subject line should always be able to complete the following sentence:

    If applied, this commit will **your subject line here**

For example:

- If applied, this commit will **refactor subsystem X for readability**
- If applied, this commit will **update getting started documentation**
- If applied, this commit will **remove deprecated methods**
- If applied, this commit will **release version 1.0.0**
- If applied, this commit will **merge pull request #123 from user/branch**

Notice how this doesn't work for the other non-imperative forms:

- If applied, this commit will **fixed bug with Y**
- If applied, this commit will **changing behavior of X**
- If applied, this commit will **more fixes for broken stuff**
- If applied, this commit will **sweet new API methods**

Writing commit message body content
-----------------------------------

**The message body should be wrapped at 72 character line lengths**, and contain lists or paragraphs that explain the code changes.

The commit message body describes:

- What the original issue was; the reader shouldn't have to look at JIRA to understand what prompted the code change.
- What the changes actually are and why they were made.
- What the limitations of the code are.
  This is especially useful for future debugging.

Git commit messages *are not* used to document the code and tell the reader how to use it.
Documentation belongs in code comments, ``docstrings`` and documentation files.

If the commit is trivial, a multi-line commit message may not be necessary.
Conversely, a long message might suggest that the :doc:`commit should be split <best_practices>`.
The code reviewer is responsible for giving feedback on the adequacy of commit messages.

The `OpenStack docs have excellent thoughts on writing great commit messages <https://wiki.openstack.org/wiki/GitCommitMessages#Information_in_commit_messages>`__.
