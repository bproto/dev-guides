###############################
Configuration & Recommendations
###############################

.. caution::
    This guide has been based on `LSST <https://developer.lsst.io/index.html>`__'s Git guide.
    Please respect the creators and contributor of the original guide as they have done an external work.

This page collects advice for configuring Git for project development.

Some of these configurations are mentioned in the :doc:`flow`.
Such configurations are labeled on as page as **required** (as opposed to recommended practices).

Learning Git
============

If you're new to Git, there are many great learning resources, such as

* `Git's online docs <http://git-scm.com/doc>`__, and the associated online `Pro Git <http://git-scm.com/book/en/v2>`__ book by Scott Chacon and Ben Staubb.
* `GitHub Help <https://help.github.com>`__, which covers fundamental git usage too.
* `StackOverflow <http://stackoverflow.com/questions/tagged/git?sort=frequent&pageSize=15>`__.

.. _git-setup-employer-email:

Use your *employer-hosted* email with Git & GitHub
==================================================

We use Git commit authorship metadata to audit copyrights project code.

In Git (required configuration)
-------------------------------

Ensure that Git is set up to use your *employer-hosted* email address in the :file:`~/.config/git/config` file.
You can do this from the command line:

.. prompt:: bash

   git config --global user.name "Your Name"
   git config --global user.email "your_email@employer.com"

You might not want to use your employee's email when you contribute to other projects.
Since Git 2.13, you can set `directory-based conditional configurations <https://blog.github.com/2017-05-10-git-2-13-has-been-released/#conditional-configuration>`__.
For example, if you always work on *employer projects* from your :file:`~/employer/` directory, you can add this to the bottom of your :file:`~/.config/git/config` file:

.. code-block:: ini

   [includeIf "gitdir:~/employer/"]
   path = .gitconfig-employer

Then in a :file:`~/.gitconfig-employer` file, set your employer configurations:

.. code-block:: ini

   [user]
   name = Your Name
   email = your_email@employer.com

Whenever you work on a repository inside :file:`~/employer/`, the configurations in :file:`~/.gitconfig-employer` are applied, and possibly override previous configurations from the :file:`~/.config/git/config` file.

On GitLab
---------

Likewise, in your `GitLab profile commit email settings <https://gitlab.com/-/profile#s2id_user_commit_email>`__, add your *employer-hosted* email.

We recommend that you set this employee email as your **Primary GitLab** email address.
This step ensures that Git commits you make `directly on GitLab.com <https://docs.gitlab.com/ee/user/project/web_ide/>`__ (such as quick documentation fixes) and merges made via the 'big blue button' have proper authorship metadata.

.. _git-setup-plain-pushes:

Configure 'plain' pushes in Git (required for Git prior to v2.0)
================================================================

Ensure that ``git push`` only pushes your currently checked-out branch by running this command:

.. prompt:: bash

   git config --global push.default simple

This command modifies :file:`~/.config/git/config`.

.. note::

   This behavior is the default for Git v2.0 and later.

   In earlier versions of Git, ``push.default=matching`` was the default.
   See the `git-config <https://git-scm.com/docs/git-config>`__ documentation for details.

Set up Two-Factor Authentication (2FA) for GitLab
=================================================

We encourage you to enable `Two-Factor Authentication (2FA) for GitLab <https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html>`__ through your `account settings <https://gitlab.com/-/profile/account>`__.
2FA means that you'll have to enter an authentication code when logging into GitLab.com from a new computer.
Apps like  `Authy <https://www.authy.com>`__, and the ``Google Authenticator App`` can help you generate these authentication codes.
When pushing commits with a 2FA-enabled account, you'll use a personal access token instead of your password.
You can `create and revoke tokens from your account settings <https://gitlab.com/-/profile/account>`__.
To help you automatically authenticate when pushing to GitLab, we encourage you to follow the next step and enable a credential helper.

Set up a Git credential helper
==============================

Rather than entering your GitLab username and password (or 2FA access token) every time you push, you can set up a Git credential helper to manage this for you.

**Mac users** can use the secure macOS keychain:

.. prompt:: bash

   git config --global credential.helper osxkeychain

**Linux users** can use a credential *cache* to temporarily keep credentials in memory.
To have your credentials cached for 1 hour (3600 seconds):

.. prompt:: bash

   git config --global credential.helper 'cache --timeout=3600'

**Linux users can alternatively** have their `credentials stored on disk <http://git-scm.com/docs/git-credential-store>`__ in a :file:`~/.git-credentials` file.
Only do this for machines where you can ensure some level of security.

.. prompt:: bash

   git config --global credential.helper store

Once a credential helper is enabled, the next time you ``git push``, you will add your credentials to the helper.

Remember that if you have 2FA enabled, you will create and use a `personal access token <https://gitlab.com/-/profile/account>`__ instead of your GitLab password.

If you find that ``git push`` is not working but also not asking you for credentials, you may need to manually insert the username/password or token into the credential store or macOS keychain.

Tune your shell for Git
=======================

You can build an effective development environment and workflow by tuning your Git setup.
Here are some ideas:

1. `Add git status to your prompt <http://git-scm.com/book/en/v2/Git-in-Other-Environments-Git-in-Bash>`__.
2. `Enable shell autocompletion <http://git-scm.com/book/en/v2/Git-in-Other-Environments-Git-in-Bash>`__.
3. `Craft aliases for common workflows <http://git-scm.com/book/en/v2/Git-Basics-Git-Aliases>`__.

Set up your editor
==================

You'll want to configure your preferred editor (or its command line hook) as your Git editor.
For example:

.. prompt:: bash

   git config --global core.editor "atom --wait"
   git config --global core.editor "code --wait"
   git config --global core.editor "subl -n -w"

See `GitHub's help for setting up VS Code and Sublime Text as Git editors <https://help.github.com/articles/associating-text-editors-with-git/>`__.

Useful Git aliases and configurations
=====================================

You can craft custom Git commands (aliases) in your :file:`~/.config/git/config` to refine your workflow.
When you run an alias (``git <alias> [arguments]``) the alias's name is effectively replaced with the alias's content in the command line statement.

Here are some aliases try in :file:`~/.config/git/config`:

.. use quotes on alias contents to make Pygments highlighter happy

.. literalinclude:: samples/config.ini
   :language: ini
