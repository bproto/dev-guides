##########
Git Guides
##########

.. toctree::
   :maxdepth: 1
   :hidden:

   setup.rst
   repo.rst
   commit_message.rst
   best_practices.rst

- :doc:`setup`
- :doc:`repo`
- :doc:`commit_message`
- :doc:`best_practices`