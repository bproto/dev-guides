======================
Formatting Conventions
======================

Text wrapping
-------------

When writing reST documentation in Python docstrings, documentation lines should be wrapped at lengths of 110 characters for :ref:`consistency with our Python Style Guide <style-guide-py-line-length>`.

.. NOTE: ls.st/rfc-107

For reStructuredText documents (e.g., ``.rst`` files), reST doesn't care about line formatting.
Emacs users, for example, are free to use hard-wrap formatting lines at 72 characters if that helps you write docs.
Whenever possible, we *encourage* you to use soft-wrapping for your text.
This allows others format text columns in their editors as they wish.
As well, the GitHub.com code editor does not have hard-wrap auto-formatting.
Those making doc edits on GitHub.com will tend to use soft-wrap by default (see '`GitLab Web IDE`_').

.. _GitLab Web IDE: https://docs.gitlab.com/ee/user/project/web_ide/

When using soft-wrap formatting, you might **write one sentence per line** (i.e., put a line break after each sentence).
As a writer, this has the advantage of making it easier to check the rhythm of your writing, including sentence lengths.
Shorter sentences are easier to read.
`One-sentence-per-line`_ is also semantically correct in the sense of Git.

.. _One-sentence-per-line: https://xkcd.com/1285/

If using this style with Emacs, you may find these configuration settings useful:

.. code-block:: emacs-lisp

   (add-hook 'rst-mode-hook 'turn-on-visual-line-mode)
   (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
   ;; or (setq visual-line-fringe-indicators '(nil right-curly-arrow))
   ;; or (setq visual-line-fringe-indicators '(left-curly-arrow nil))

This changes Emacs's default of soft-wrapping at the width of the display frame to instead soft-wrapping at the nearest whitespace.
You may find this makes one-sentence-per-line text more readable (or not).
The last three lines control whether to mark wrapped lines with fringe indicators; some prefer to see those indicators, and others find them visual clutter.
The Emacs default for Visual Line mode is to not use fringe indicators.

In addition, we place a **single blank line** between all content blocks, such as directives, paragraphs and lists.

Indentation
-----------

ReStructuredText should be indented consistently with the context, which generally means taking visual alignment cues rather than adhering to a fixed indent width.

In directives, align to the directive's name:

.. code-block:: rst

   .. code-block:: python

      print('hello world')

In lists, align naturally with the text:

.. code-block:: rst

   - First item.

     Another paragraph for the first item.
   - Second item.

Note how that alignment adapts to numbered lists:

.. code-block:: rst

   1. First item.

      Another paragraph for the first item.
   2. Second item.

For :ref:`argument lists in Python docstrings <py-docstring-parameters>` we indent descriptions by four spaces:

.. code-block:: rst

   Parameters
   ----------
   x_coord : float
       Particle's x-coordinate.
   y_coord : float
       Particle's y-coordinate.

Encoding and special characters
-------------------------------

reStructuredText source files should be encoded as UTF-8 unicode.
This means that special characters such as en (–) and em (—) dashes can just be written as such.
We do run a variant of `smartypants`_ in an attempt to convert ``--`` and ``---`` into en and em dashes, respectively, and to convert dumb quotes (``"``) into "typographic" quotes.

.. _smartypants: http://daringfireball.net/projects/smartypants/

