##########
RST Guides
##########

How reStructuredText (reST) is written for project documentation.

.. toctree::
   :maxdepth: 1
   :hidden:

   primer.rst
   formatting.rst

- :doc:`primer`
- :doc:`formatting`