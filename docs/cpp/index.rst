##########
C++ Guides
##########

.. toctree::
   :maxdepth: 1
   :hidden:

   naming.rst
   comments.rst
   formatting.rst
   recommendations.rst
   structure.rst
   testing.rst
   boost.rst

Preface
=======

The communal nature of creating project software necessitates simplicity and elegance in the crafting of code.
Since a piece of code may be a collaboration without readability and comprehensibility the result of the collaboration may not preserve integrity of design intent.
Without simplicity, it might not be possible to make a judgment of that integrity.

Preserving integrity of design intent is important.
The creation of a piece of software is an exercise in developing a consistent set of descriptions (requirements, design, code, tests, manuals) that preserve and manage the evolution of the intent of that software throughout its lifetime.
This gains more importance as the key form of these descriptions is an operational (imperative) form, which will decide how a system will react to specified (an, in some cases, unexpected) external stimuli.

This document is strongly based on (verily, virtually identical to) the `CARMA <http://www.mmarray.org/workinggroups/computing/cppstyle.html>`_ [Pound]_ C++ Coding Standards which, in turn, was strongly based on Geosoft [Geosoft]_ and `ALMA C++ Coding Standards <https://science.nrao.edu/facilities/alma/aboutALMA/Technology/ALMA_Computing_Memo_Series/0009/2001-06-06.pdf>`_ [Bridger2001]_.
The layout section of this document is also based on the `Google C++ Style Guide <https://google.github.io/styleguide/cppguide.html>`_ [Google]_.
We have taken the CARMA HTML document and changed it in places to match our needs.
CARMA, Geosoft, ALMA and Google retain their respective copyrights where appropriate.

Introduction
============

This document lists C++ coding recommendations common in the C++ development community.
The recommendations are based on established standards collected from a number of sources, individual experience, local requirements/needs, as well as suggestions given in [McConnell2004]_, [Henricson1992]_, [Henricson1992]_, [Hoff2008]_ and [Google]_.

While a given development environment (IDE) can improve the readability of code by access visibility, color coding, automatic formatting and so on, the programmer should never rely on such features.
Source code should always be considered larger than the IDE it is developed within and should be written in a way that maximizes its readability independent of any IDE.

General Recommendations
=======================

**Remember, we are writing code for humans to read, not computers**

At some point, someone unfamiliar with your code (often a future you) will have to examine it, typically to fix a bug or upgrade it.
These tasks are made much simpler if the code is easily readable and well-documented.

**We are writing C++17**

We follow the official: International Standard ISO/IEC 14882:2017(E) - Programming Language C++, without any compiler specific extensions.

.. note::

   We use compilers provided by a specific :ref:`Conda environment <platforms-environment>` executing on a defined :ref:`baseline platform <platforms-baseline>`.
   This environment provides complete support for C++17, but --- in the case of compiler bugs --- the allowed set of C++17 features is those which can be shown to work properly in this reference environment.

The C++17 standard adds a number of useful features such as nested namespace definitions, structured binding declarations, initializers in ``if`` and ``switch`` statements, class template argument deduction, ``std::optional``, ``std::any``, ``std::filesystem``, and ``std::variant``.
A parallel algorithms library has also been added, although use of this should be vetted, as we typically use one-core-per-process parallelization.
``std::auto_ptr`` has been removed and should typically be replaced by ``std::unique_ptr``.

When using C++17 features, be careful about readability for developers familiar only with C++14.

If you find that any Style Guide rules conflict with C++17 best practices, such as those in the `C++ Core Guidelines`_, please propose updates.

.. _C++ Core Guidelines: https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md

**Some rules MAY be violated under certain circumstances**

See :ref:`Deviating from the DM Style Guides <style-guide-deviations>`.

**Object orientation SHOULD be used in your programs**

- Do not just code C style in C++.

- Make a real class for any behavior on a data structure, do not make a ``struct`` for the data and separate functions to operate on it.

- ``struct`` s are appropriate only for cases needing very lightweight data structure and no behavior.

- Avoid overly complex inheritance hierarchies, more than 3 levels should be a warning sign (except in Frameworks).

- Use inheritance to specialize behavior for the same or similar data, use templates to specialize data for the same behavior.

- Avoid multiple inheritance, and only use when it is for completely distinct/disjoint considerations (such as application role versus persistence container type).

- You may overload member functions but try to do so only where required (virtual functions) or you need to vary the parameter list.

- Keep functions short and with a single purpose.

Sections
========

- :doc:`naming`
- :doc:`comments`
- :doc:`formatting`
- :doc:`recommendations`
- :doc:`structure`
- :doc:`testing`
- :doc:`boost`

References
==========

.. [Pound] Pound, M. W., Amarnath, N.S., & Teuben, P.J. CARMA C++ Programming Style Guidelines. Available online at http://www.mmarray.org/workinggroups/computing/cppstyle.html.

.. [Geosoft] Geosoft C++ Programming Style Guide. Available on-line at http://geosoft.no/development/cppstyle.html.

.. [Bridger2001] Bridger, Alan, Brooks, M., & Pisano, Jim. C++ Coding Standards, Revision 3, ALMA Computing Memo 0009 (Atacama Large Millimeter Array), 2001. Available on-line at https://science.nrao.edu/facilities/alma/aboutALMA/Technology/ALMA_Computing_Memo_Series/0009/2001-06-06.pdf

.. [McConnell2004] McConnell, Steve. Code Complete, 2nd Edition, (Redmond, WA: Microsoft Press), 2004. See http://www.stevemcconnell.com/cc.htm.

.. [Henricson1992] Henricson, M., & Nyquist, E. Programming in C++, Rules and Recommendations, (Alvsjo, Sweden: Ellemtel Telecommmunication Systems Laboratories), 1992. Available on-line at:
 http://www.mmarray.org/workinggroups/computing/ellemtel.pdf

.. [Hoff2008] Hoff, Todd. C++ Coding Standard, 2008. Available on-line at:
   http://www.possibility.com/Cpp/CppCodingStandard.htm

.. [Google] Google C++ Style Guide, 2017. Available on-line at:
   https://google.github.io/styleguide/cppguide.html