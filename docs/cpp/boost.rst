###########
Using Boost
###########

.. _cpp_using_boost:

A Boost library may be used only if:

1. the desired effect cannot be accomplished with a C++17 standard language feature,
2. a C++ standard library equivalent is either unavailable or unusable with minimum required compiler version for a certain C++ project, and
3. there is no 3rd-party equivalent that is much better comparing with a chosen Boost library.

In particular, the following Boost libraries are no longer accepted as they have standard equivalents in gcc 10.2.1 and above:

``any``
        use ``<any>``

``array``
        use ``<array>``

``bind``
        prefer C++14 lambda functions instead, but use ``std::bind`` from ``<functional>`` if you must

``cstdint``
        use ``<cstdint>``

``filesystem``
        use ``<filesystem>``

``format``
        use `fmtlib/fmt <https://github.com/fmtlib/fmt>`__ library

``lambda``
        use C++14 lambda functions

``lexical_cast``
        use ``std::to_string``, ``std::stoi``, ``std::stod`` etc.

``math``
        use ``<cmath>`` wherever possible

``noncopyable``
        use ``= delete`` on the copy constructor and assignment operator

``python``
        use `pybind11 <https://github.com/pybind/pybind11>`__ library

``random``
        use ``<random>``

``ref``
        use ``std::forward`` and universal / forwarding references (or ``std::ref`` if you must)

``smart_ptr``
        use ``std::shared_ptr`` and ``std::unique_ptr`` (and its array specialization) from ``<memory>`` instead of ``boost::shared_ptr``, ``boost::scoped_ptr`` and ``boost::scoped_array``

``static_assert``
        use C++14 ``static_assert``

``tuple``
        use ``<tuple>``

``type_traits``
        use ``<type_traits>``

``unordered_map``
        use ``<unordered_map>``

``variant``
        use ``<variant>``

Certain Boost libraries are recommended: they should be used whenever applicable in preference to any other method of accomplishing the same effect.
Among others, this category includes:

* ``current_function``
* ``regex``
* ``test``
* ``throw_exception``

Most other Boost libraries may be used after appropriate design review.
Particular caution should be used when the library involves substantial template metaprogramming or requires linking.
Among others, the following libraries fall into the extra-caution category:

* ``Fusion``
* ``Hana``
* ``MPL``

Certain Boost libraries conflict with our standard ways of doing things, are inappropriate for our source code, are insufficiently developed or well-maintained, or have been found to be excessively complicated.
These are not allowed without special permission.

* ``config``
* ``serialization``
