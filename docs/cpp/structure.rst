#####################
Projects organization
#####################

Project directory structure
===========================

We have 4 obligatory directories in each project.

.. list-table::
   :header-rows: 1
   :widths: 20 50

   * - Directory
     - Purpose

   * - include/<namespace...>/
     - Headers that are public relatively to module and can be used by other modules.

   * - ih
     - Headers that are used as internal headers for special access to module.
       Folder is optional and can be missed in some plugins

   * - mocks/<namespace...>/
     - Mock headers of those that are defined in ``include`` folder and are public relatively to module and can be used by other modules.

   * - src
     - All source files/headers of module implementation.
       Nobody should include files from this directory to other modules

   * - tests
     - All source files/headers for module tests.

File naming
===========

We use only \*.hpp/\*.cpp extensions.

Lower-case names are used for files.
Words are separated by underscore (_).
Each file name has prefix that represents relation to concrete module.
Each file inside single module has same prefix.
Name should represent file content.
Single class/struct/union/enum - single file.

.. code-block:: shell

    # Use this
    vlog_sm_file_header.hpp

.. code-block:: shell

    # Instead of this
    fileheader.hpp
    fileHeader.hpp
    file_header.hpp
    VlogSmFileHeader.hpp

File structure
==============

Each file should start with copyright mark.
This mark contains year when file was created.
Copyright is followed by include guard

Typical \*.hpp file structure is:

- Copyright
- Include guard (begin)
- Includes
- Namespace (begin)
- File main content: class definition, free functions, enums, etc.
- Namespace (end)
- Include guard (end)

Typical \*.cpp file structure is:

- Copyright
- Includes
- Namespace (begin)
- File main content: class declaration, free functions, enums, etc.
- Namespace (end)

Empty single line is required at the end of any file.
