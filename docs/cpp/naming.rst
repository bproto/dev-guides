.. role:: icpp(code)
   :language: cpp

##################
Naming Conventions
##################

.. caution::
    This guide has been based on `LSST <https://developer.lsst.io/index.html>`__'s C++ API documentation guide.
    Please respect the creators and contributor of the original guide as they have done an external work.

General Limitations
===================

.. warning::

    Identifiers should consist of English words.
    Other languages, jargons, jokes or abuses are forbidden.

The fundamental quantity being described should appear first in the name, with modifiers concatenated afterward.
A rule of thumb is to ask what the units of the quantity would be, and make sure that quantity appears first in the name.

- ``dateObs``, not ``obsDate`` for a quantity that fundamentally is a date/time of significance.
- ``timeObsEarliest`` (or, ``timeObsFirst``), not ``earliestObsTime``.

Use care to select the most meaningful name to represent the quantity being described

- ``imageMean`` not ``pixelMean`` if we are talking about the mean value of an image, not repeated measurements of a pixel.

Names should not explicitly include units

- ``skyBackground`` not ``skyADU`` to indicate the sky background level.
- ``expMidpoint`` rather than ``taiMidPoint``.
- ``timeRange`` not ``taiRange``.

Abbreviations
-------------

Abbreviations in names SHOULD be avoided.

.. code-block:: cpp

   computeAverage();     // NOT:  compAvg();

There are two types of words to consider.
First are the common words listed in a language dictionary.
These must never be abbreviated.
For example, write:

- ``command`` instead of ``cmd``;
- ``copy`` instead of ``cp``;
- ``point`` instead of ``pt``;
- ``compute`` instead of ``comp``;
- ``initialize`` instead of ``init``.

Then there are domain specific phrases that are more naturally known through their abbreviations/acronym.
These phrases should be kept abbreviated.
For example, write:

- ``html`` instead of ``HypertextMarkupLanguage``;
- ``cpu`` instead of ``CentralProcessingUnit``;
- ``ccd`` instead of ``ChargeCoupledDevice``.

Acronyms, Initialisms, Alphabetisms
-----------------------------------

In English abbreviations have been written with a full stop/period/point in place of the deleted part to show the ellipsis of letters, although the colon and apostrophe have also had this role.
Such punctuation is diminishing with the belief that the presence of all-capital letters is sufficient to indicate that the word is an abbreviation.
But, in code, identifiers with acronyms in all-capital letters style are much less readable.
That is why we propose to keep using pascal style for acronyms.

For example we choose DomObject instead of DOMObject (where DOM is Document Object Model).

.. code-block:: cpp

    // Use this
    enum class Enum
    {
        Sfinae
    };

.. code-block:: cpp

    // Instead of this
    enum class Enum
    {
        SFINAE
    };

Uppercase acronyms are not allowed for enumerators too.

.. code-block:: cpp

    // Use this
    enum class Enum
    {
        Sfinae
    };

.. code-block:: cpp

    // Instead of this
    enum class Enum
    {
        SFINAE
    };

This rule concerns only established acronyms, initialisms and alphabetisms.
Moreover such things should be used sparingly, and limited to very common usages in the relevant community.

.. warning:: Remember, using of your own acronyms, initialisms or alphabetisms is forbidden.

This means that obscure abbreviations should be avoided too: clarity is probably more important than brevity.

- ``apertureDiam`` would be better than ``apDia``

Booleans
--------

**Names for boolean variables and methods SHOULD be obviously boolean.**

Examples of good names include:

.. code-block:: cpp

   bool isSet, isVisible, isFinished, isFound, isOpen;
   bool exists();
   bool hasLicense(), canEvaluate(), shouldSort()

Common practice in the C++ development community and partially enforced in Java.
Using the ``is`` prefix can highlight a common problem of choosing bad boolean names like ``status`` or ``flag``.
``isStatus`` or ``isFlag`` simply doesn't fit, and the programmer is forced to choose more meaningful names.

**Negated boolean variable names MUST be avoided.**

.. code-block:: cpp

   bool isError;    // NOT: isNoError
   bool isFound;    // NOT: isNotFound

The problem arises when such a name is used in conjunction with the logical negation operator as this results in a double negative.
It is not immediately apparent what ``isNotFound`` means.

Complement names
----------------

Complement names MUST be used for complement operations, as this reduces complexity by symmetry.

For example:

- ``get/set``,
- ``add/remove``,
- ``create/destroy``,
- ``start/stop``,
- ``insert/delete``,
- ``increment/decrement``,
- ``old/new``, ``begin/end``,
- ``first/last``,
- ``up/down``,
- ``min/max``,
- ``next/previous``,
- ``old/new``,
- ``open/close``,
- ``show/hide``,
- ``suspend/resume``,
- etc..

Shorternings
------------

It is recommended to use well-known abbreviations of given domain instead of their full equivalents.
Use of one- or two-letter identifiers and shortenings in words is forbidden with exception of worldwide known.
However developer (and maintainer) should be consistent in one module bounds.

Examples of allowed shortenings are shown in tables below (list is not closed).

.. list-table::
   :header-rows: 1
   :widths: 60 40

   * - Shortering
     - Meaning

   * - i, j, k, idx
     - Loop counters, Index

   * - x, y
     - Coordinates

   * - e, ex
     - Exception

   * - init
     - Initialize

   * - it, itEnd, it\* (itObject, itValues)
     - Iterator

   * - p, \*Ptr (pObject, EntityPtr)
     - Pointer

   * - ref, \*Ref (objectRef, valuesRef)
     - Reference

   * - id
     - Identifier

   * - opt, \*Opt, opt\* (optObject, EntityOpt)
     - Optional

Identifiers
===========

.. table::
    :widths: 60 10 60

    +--------------------------+----------+-------------------------------------------+
    | Identifier               | Style    | Example                                   |
    +==========================+==========+===========================================+
    | Namespace                | Pascal   | :icpp:`namespace DataModel`               |
    +--------------------------+----------+-------------------------------------------+
    | Class                    | Pascal   | :icpp:`class RichKaby;`                   |
    +--------------------------+          +-------------------------------------------+
    | Struct                   |          | :icpp:`struct RichKaby;`                  |
    +--------------------------+          +-------------------------------------------+
    | Union                    |          | :icpp:`union RichKaby;`                   |
    +--------------------------+----------+-------------------------------------------+
    | Enumeration              | Pascal   | .. code-block:: cpp                       |
    |                          |          |                                           |
    | Enumerator               |          |     enum class Language                   |
    |                          |          |     {                                     |
    |                          |          |         C,                                |
    |                          |          |         Cpp                               |
    |                          |          |     };                                    |
    |                          |          |                                           |
    +--------------------------+----------+-------------------------------------------+
    | Interface                | Pascal   | :icpp:`class SomeObject;`                 |
    +--------------------------+          +-------------------------------------------+
    | Implementation           |          | :icpp:`class SomeObjectImpl;`             |
    +--------------------------+----------+-------------------------------------------+
    | Class method             | Camel    | :icpp:`int displayLine();`                |
    +--------------------------+          +                                           +
    | Free function            |          |                                           |
    +--------------------------+----------+-------------------------------------------+
    | Local variable           | Camel    | :icpp:`int lineSize;`                     |
    +--------------------------+----------+-------------------------------------------+
    | Global variable          | Camel    | :icpp:`int g_lineSize;`                   |
    +--------------------------+----------+-------------------------------------------+
    | Class field              | Camel    | :icpp:`int m_lineSize;`                   |
    +--------------------------+----------+-------------------------------------------+
    | Class static field       | Camel    | :icpp:`int ms_lineSize;`                  |
    +--------------------------+----------+-------------------------------------------+
    | Static variable          | Camel    | :icpp:`int s_lineSize;`                   |
    +--------------------------+----------+-------------------------------------------+
    | Function parameter       | Camel    | :icpp:`int _lineSize`                     |
    +--------------------------+----------+-------------------------------------------+
    | Template type parameter  | Pascal   | :icpp:`template< typename _VisitorType >` |
    +--------------------------+----------+-------------------------------------------+
    | Template value parameter | Camel    | :icpp:`template< int _someValue >`        |
    +--------------------------+----------+-------------------------------------------+
    | Include guard            | Capitals | :icpp:`#define __BPROTO_SRC_CAST_HPP__;`   |
    +--------------------------+----------+-------------------------------------------+
    | Macro                    | Capitals | :icpp:`#define BPROTO_CHECK;`              |
    +--------------------------+----------+-------------------------------------------+

Scope Designation
=================

Names of local and non-constant static variables, class members, function and template arguments are prepended with prefix that designates scope.

.. table::
    :widths: 60 10 60

    +-------------------------------+--------+---------------------------------------------+
    | Scope                         | Prefix | Example                                     |
    +===============================+========+=============================================+
    | Local variable                | None   | :icpp:`int lineSize{ 0 };`                  |
    +-------------------------------+        +---------------------------------------------+
    | Static constant variable      |        | :icpp:`static const int lineSize{ 0 };`     |
    +-------------------------------+        +---------------------------------------------+
    | Static non-constant variable  |        | :icpp:`static const int lineSize{ 0 };`     |
    +-------------------------------+        +---------------------------------------------+
    | Static ``constexpr`` variable |        | :icpp:`static constexpr int lineSize{ 0 };` |
    +-------------------------------+--------+---------------------------------------------+
    | Function parameter            | \_     | :icpp:`func(int _lineSize);`                |
    +-------------------------------+        +---------------------------------------------+
    | Template type parameter       |        | :icpp:`template<typename _VisitorType>`     |
    +-------------------------------+        +---------------------------------------------+
    | Template value parameter      |        | :icpp:`template<int _someValue>`            |
    +-------------------------------+--------+---------------------------------------------+
    | Class field                   | m\_    | :icpp:`int m_lineSize;`                     |
    +-------------------------------+--------+---------------------------------------------+
    | Class static field            | ms\_   | :icpp:`int ms_lineSize;`                    |
    +-------------------------------+--------+---------------------------------------------+
    | Global variable               | g\_    | :icpp:`int g_lineSize;`                     |
    +-------------------------------+--------+---------------------------------------------+

Methods and Functions
=====================

It is recommended to use predefined naming schemes for common actions.
Predefined naming schemes can be found below.
However if your method name can't be fit into those schemes, there are some rules that can help you.

- Names representing methods or functions SHOULD naturally describe the action of the method (and so will usually start with a verb).

- Names for methods that return new objects MAY start with past-tense verbs.

  It is sometimes useful to pair a mutator with a ``const`` method that returns a mutated copy of the callee.
  When it is, the imperative verb in the name of the mutator MAY be changed to the past tense to make the distinction clear.
  For example:
  
  .. code-block:: cpp
  
      Box b;
      b.dilateBy(a);           // b is modified
      Box c = b.dilatedBy(a);  // a modified copy of b is assigned to c

Methods for creating new objects
--------------------------------

.. table::
    :widths: 20 40 50

    +---------------+-----------------------------------------+------------------------------------------------------------+
    | Naming Scheme | Example                                 | Description                                                |
    +===============+=========================================+============================================================+
    | create\*      | .. code-block:: cpp                     | Method that creates entity objects (e.g. factory methods). |
    |               |                                         | Usually return object and do not save it internally.       |
    |               |     EntityPtr createEntity();           |                                                            |
    |               |     std::unique_ptr<Node> createNode(); | Should be used in most cases.                              |
    |               |                                         |                                                            |
    +---------------+-----------------------------------------+------------------------------------------------------------+
    | build\*,      | .. code-block:: cpp                     | Method creates utility objects.                            |
    |               |                                         | Can be used to distinct methods within scope (class).      |
    | make\*,       |     SearcherPtr provideSearcher();      |                                                            |
    |               |                                         | Prefix create\* can be used if you prefer.                 |
    | provide\*     |                                         |                                                            |
    +---------------+-----------------------------------------+------------------------------------------------------------+
    | clone\*       | .. code-block:: cpp                     | Clone method (as exception from create\*).                 |
    |               |                                         |                                                            |
    |               |     ObjectPtr clone();                  |                                                            |
    |               |                                         |                                                            |
    +---------------+-----------------------------------------+------------------------------------------------------------+

Methods for returning objects
-----------------------------

.. table::
    :widths: 25 30 50

    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+
    | Naming Scheme | Example                                   | Description                                                                       |
    +===============+===========================================+===================================================================================+
    | get\*         | .. code-block:: cpp                       | Method returns non-null object by reference.                                      |
    |               |                                           | if object is not present by some reasons, should throw an exception.              |
    |               |     Object const & getObject() const;     |                                                                                   |
    |               |                                           | Should have O(1) complexity.                                                      |
    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+
    | tryGet\*      | .. code-block:: cpp                       | Method returns pointer to object that not always exists.                          |
    |               |                                           |                                                                                   |
    |               |     Object const * tryGetObject() const;  | Returned pointer must always be non-owning.                                       |
    |               |                                           |                                                                                   |
    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+
    | take\*        | .. code-block:: cpp                       | Method returns non-null non-constant object by reference.                         |
    |               |                                           | Prefer this method to overloaded version of get\* method.                         |
    |               |     Object const & takeObject() const;    |                                                                                   |
    |               |                                           |                                                                                   |
    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+
    | tryTake\*     | .. code-block:: cpp                       | Method returns by non-constant pointer object that not always exists.             |
    |               |                                           | Prefer this method to overloaded version of tryGet\* method.                      |
    |               |     Object const * tryTakeObject() const; |                                                                                   |
    |               |                                           | Returned pointer must always be non-owning.                                       |
    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+
    | find\*        | .. code-block:: cpp                       | Method searches object and returns optional value (pointer or ``std::optional``). |
    |               |                                           |                                                                                   |
    |               |     Object const * findObject() const;    | Method should have O(1)\* or O(logN) complexity.                                  |
    |               |     Object * findObject();                |                                                                                   |
    |               |                                           | Returned pointer must always be non-owning.                                       |
    +---------------+-------------------------------------------+-----------------------------------------------------------------------------------+

Methods for returning statuses/flags/props
------------------------------------------

.. table::
    :widths: 25 30 50

    +---------------+--------------------------------------+-------------------------------------------------------+
    | Naming Scheme | Example                              | Description                                           |
    +===============+======================================+=======================================================+
    | is\*          | .. code-block:: cpp                  | Method answers questions like:                        |
    |               |                                      | * is an object of a specific kind/type?               |
    |               |     bool isActive() const;           | * has it some state (full, initialized, etc)?         |
    |               |     bool isPort() const;             | Should have O(1) complexity.                          |
    |               |                                      |                                                       |
    +---------------+--------------------------------------+-------------------------------------------------------+
    | has\*         | .. code-block:: cpp                  | Method answers if object has a certain props.         |
    |               |                                      |                                                       |
    |               |     bool hasChildren() const;        |                                                       |
    |               |                                      |                                                       |
    +---------------+--------------------------------------+-------------------------------------------------------+
    | are\*         | .. code-block:: cpp                  | Comparison methods.                                   |
    |               |                                      |                                                       |
    |               |     bool areEqual(...);              | Use either free function with 2 (or more) parameters. |
    |               |     bool Class::equalsTo(...) const; | Second form should be used in case of class method.   |
    |               |                                      |                                                       |
    +---------------+--------------------------------------+-------------------------------------------------------+
    | get\*         | .. code-block:: cpp                  | Methods returns values of some props or flags.        |
    |               |                                      |                                                       |
    |               |     int getSize() const;             |                                                       |
    |               |                                      |                                                       |
    +---------------+--------------------------------------+-------------------------------------------------------+

Methods for modifying objects
-----------------------------

.. table::
    :widths: 25 30 50

    +---------------+-------------------------------------+--------------------------------------------------------------------+
    | Naming Scheme | Example                             | Description                                                        |
    +===============+=====================================+====================================================================+
    | set\*         | .. code-block:: cpp                 | Method modifies single or several props.                           |
    |               |                                     |                                                                    |
    |               |     void setName(...);              |                                                                    |
    |               |     void setMatched();              |                                                                    |
    |               |                                     |                                                                    |
    +---------------+-------------------------------------+--------------------------------------------------------------------+
    | reset\*       | .. code-block:: cpp                 | Method can act in two ways:                                        |
    |               |                                     | #. cleans props in case of empty parameters,                       |
    |               |     void resetDesign(...);          | #. or modifies props with new values passed to method.             |
    |               |     void resetDesign();             |                                                                    |
    |               |                                     |                                                                    |
    +---------------+-------------------------------------+--------------------------------------------------------------------+
    | add\*         | .. code-block:: cpp                 | Method creates new object, saves to internal collection.           |
    |               |                                     | And finally return reference to newly created object.              |
    |               |     Object const & addObject(...);  |                                                                    |
    |               |     void addObject(Object const &); | Or method just adds already created object to internal collection. |
    |               |                                     |                                                                    |
    +---------------+-------------------------------------+--------------------------------------------------------------------+
    | ensure\*      | .. code-block:: cpp                 | Same as add\*, but used to explicitly point that cache is used.    |
    |               |                                     | Cache can be used to avoid duplicates or to optimize algorithm.    |
    |               |     Node const & ensureNode(...);   |                                                                    |
    |               |                                     | If object with same properties already exist, it will be returned. |
    |               |                                     | No new instance will be created in this case.                      |
    +---------------+-------------------------------------+--------------------------------------------------------------------+
    | try\*         | .. code-block:: cpp                 | Method performs action that fails (relatively) often.              |
    |               |                                     | The fallback function call might be followed after failure.        |
    |               |     void tryEmplace(...);           |                                                                    |
    |               |                                     |                                                                    |
    +---------------+-------------------------------------+--------------------------------------------------------------------+
