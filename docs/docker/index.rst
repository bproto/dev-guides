============
Docker Guide
============

.. caution::
    This guide has been fully based on LLVM's `A guide to Dockerfiles for building LLVM <https://llvm.org/docs/Docker.html>`__.

Introduction
============

You can find a number of sources to build docker images in ``docker`` folder.
They can be used by anyone who wants to build the docker images for their own use, or as a starting point for someone who wants to write their own Dockerfiles.

Each project currently provide Dockerfiles with a variety of different images, including ``Alpine 3``, ``CentOS 7``, ``CentOS 8`` and ``Ubuntu 20.04``.

Why?
----

Docker images provide a way to produce binary distributions of software inside a controlled environment.
Having Dockerfiles to builds docker images inside a specific repo makes them much more discoverable than putting them into any other place.

Docker basics
-------------

If you've never heard about Docker before, you might find this section helpful to get a very basic explanation of it.
`Docker <https://www.docker.com/>`__ is a popular solution for running programs in an isolated and reproducible environment, especially to maintain releases for software deployed to large distributed fleets.
It uses `linux kernel namespaces <https://en.wikipedia.org/wiki/Linux_namespaces>`__ and `cgroups <https://en.wikipedia.org/wiki/Cgroups>`__ to provide a lightweight isolation inside currently running linux kernel.
A single active instance of dockerized environment is called a *docker container*.
A snapshot of a docker container filesystem is called a *docker image*.
One can start a container from a prebuilt docker image.

Docker images are built from a so-called *Dockerfile*, a source file written in a specialized language that defines instructions to be used when build the docker image (see `official documentation <https://docs.docker.com/engine/reference/builder/>`__ for more details).
A minimal Dockerfile typically contains a base image and a number of RUN commands that have to be executed to build the image.
When building a new image, docker will first download your base image, mount its filesystem as read-only and then add a writable overlay on top of it to keep track of all filesystem modifications, performed while building your image.
When the build process is finished, a diff between your image's final filesystem state and the base image's filesystem is stored in the resulting image.

Quick start
===========

Installation
------------

We use here the Debian-like system and command-line interface.

#. Install docker.

   .. prompt:: bash

      sudo apt-get install -y docker.io

#. Grant your user correct permissions.

   .. prompt:: bash

      sudo usermod -aG docker $USER

   This is a required step to avoid below message when executing any docker command:

   .. prompt:: bash

      docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/create: dial unix /var/run/docker.sock: connect: permission denied.

   .. note:: You can find more in `this SO thread <https://stackoverflow.com/questions/47854463>`__.

Usage
-----

Build image
^^^^^^^^^^^

- If Dockerfile is located at the root of the repo, you should run

  .. prompt:: bash

    docker build -t <your_name_for_this_image> .

  For example:

  .. prompt:: bash

    docker build -t "my_centos_8" .

- If Dockerfile is located in ``docker`` folder, you must run this command at the root of the repo

  .. prompt:: bash

    docker build -t <your_name_for_this_image> -f "docker/<YOUR_OS>.Dockerfile"

  For example:

  .. prompt:: bash

    docker build -t "my_centos_8" -f "docker/CentOS8.Dockerfile"

Enter image
^^^^^^^^^^^

If you need to enter into a specific docker image, you should run this

.. prompt:: bash

    docker run -it <your_name_for_this_image>

For example:

.. prompt:: bash

    docker run -it "my_centos_8"

.. note:: Please remember that all your changes will be discarded once you exist a docker image.

If you need to download and test a specific pre-build docker image from the `Docker Hub <https://hub.docker.com/>`__, you should run this

.. prompt:: bash

    docker run -it python:3-alpine /bin/sh

Mount host directories
^^^^^^^^^^^^^^^^^^^^^^

If you need to share a specific folder from your host system to a docker image, you should run this

.. prompt:: bash

    docker run -it -v <host_folder>:/<docker_image_folder> <your_name_for_this_image>

For example:

.. prompt:: bash

    docker run -it -v $(pwd):/work "my_centos_8"
    cd work
    cmake -S. -Bbuild

Remove image(s)
^^^^^^^^^^^^^^^

- To delete all docker images, you should run

  .. prompt:: bash

    docker rmi -f $(docker images -aq)

- To delete all docker containers including its volumes, you should run

  .. prompt:: bash

    docker rm -vf $(docker ps -aq)

- To make a full clean-up, you should run

  .. prompt:: bash

    docker rmi -f $(docker images -aq)
    docker rm -vf $(docker ps -aq)

  Or you can try this

  .. prompt:: bash

    docker system prune -a --volumes

.. note:: You can find more in `this SO thread <https://stackoverflow.com/questions/44785585>`__.

Clean-up cheatsheet
-------------------

Stop all containers.

.. prompt:: bash

  docker stop $(docker ps -qa)

Remove all containers.

.. prompt:: bash

  docker rm $(docker ps -qa)

Remove all images.

.. prompt:: bash

  docker rmi -f $(docker images -qa)

Remove all volumes.

.. prompt:: bash

  docker volume rm $(docker volume ls -q)

Remove all networks.

.. prompt:: bash

  docker network rm $(docker network ls -q)

*Your installation should now be all fresh and clean.*

The following commands should not output any items:

.. prompt:: bash

  docker ps -a
  docker images -a
  docker volume ls

The following command show only show the default networks:

.. prompt:: bash

  docker network ls
