##################
Visual Studio Code
##################

This page will help you configure `Visual Studio Code (VSCode) <https://code.visualstudio.com/>`__ to be consistent with coding standards and development practices.

Installation
============

Ubuntu-based Linux distros
--------------------------

Install repository and key of the VS Code package to enable auto-updating using the system's package manager.

.. prompt:: bash

    sudo apt-get install wget gpg
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
    rm -f packages.microsoft.gpg

Then update the package cache and install the package using:

.. prompt:: bash

    sudo apt-get install apt-transport-https
    sudo apt-get update
    sudo apt-get install code # or code-insiders

After VS Code is successfully installed make it a default editor for ``git``.

.. prompt:: bash

    git config --global core.editor "code --wait"

*For alternative ways of installing VS Code you can consult* |official_page|_

.. _official_page: https://code.visualstudio.com/docs/setup/linux
.. |official_page| replace:: *official Microsoft page.*

Extensions
==========

As with most advanced editors, a lot of VSCode's functionality comes from extensions, which can be installed directly from the editor GUI.

The official (Microsoft-maintained) extensions generally have quite good documentation, and this guide does not attempt to duplicate anything that can be found there.

.. note::
    Adding an extension to VSCode does not always automatically enable it when editing files remotely.
    When you first open a remote editing window on a particular server, you should check your extensions to ensure the ones you need are installed and enabled there.


Useful extensions
-----------------

`C/C++ <https://code.visualstudio.com/docs/languages/cpp>`__:
    The official C++ extension, from Microsoft.

`Python <https://code.visualstudio.com/docs/languages/python>`__:
    The official Python extension, from Microsoft.

`Python Indent <https://marketplace.visualstudio.com/items?itemName=KevinRose.vsc-python-indent>`__:
    As of this writing, VSCode's automatic indentation for Python isn't very good.
    This extension makes it a lot better - still not as good as (at least) Emacs or Sublime, but good enough that the difference is rarely noticeable.

`restructuredText <https://docs.restructuredtext.net/>`__:
    Useful for writing Sphinx docs.

`Remote Development, Remote - SSH <https://code.visualstudio.com/docs/remote/ssh>`__:
    Provides support for editing, browsing, and debugging code on a remote server from a local editor, over SSH.

`Trailing Spaces <https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces>`__:
    Highlights and/or deletes trailing spaces, which Flake8 will otherwise complain about.

General extension recommendations
---------------------------------

`Clipboard Ring <https://marketplace.visualstudio.com/items?itemName=SirTobi.code-clip-ring>`__:
    Remembers the last few things you've copy/pasted; a very limited version of what (at least) Emacs does.

`Git Graph <https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph>`__:
    An in-editor, easy-to-read version of ``git log --graph``.

`Git Lens <https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens>`__:
    In-editor ``git blame`` annotation and more.

Settings
========

Here is a suggested settings file that will configure VSCode to match approved coding styles and ignore common temporary files our builds produce.
This includes settings for some of the extensions recommended above.
The ``"rulers"`` setting doesn't affect actual indentation (unless you use the ``Rewrap`` extension or similar), but provides guides to help your own line length formatting.

**Source code**: :doc:`settings.json <settings/vs_code.json>`

.. literalinclude:: settings/vs_code.json
    :language: json
