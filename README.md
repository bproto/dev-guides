# Cpp Template Projecte Developer Guide

This repository contains the content for the [BPROTO Developer Guide, https://bproto.gitlab.io](https://bproto.gitlab.io).

Everyone who works on BPROTO is encouraged to not only *use* this guide, but also *contribute* to it.
The contribution process is outlined below.

Documentation is written in reStructuredText, built using [Sphinx](http://sphinx-doc.org), and hosted with GitLab.com pages.

**Changes to the `master` branch are automatically deployed to https://bproto.gitlab.io.**

## Contributing

1. Clone this repository:

```bash
$ git clone https://gitlab.com/bproto/bproto.gitlab.io.git
$ mv bproto.gitlab.io docs
$ cd docs
```

2. Create a branch. This can either be an informal user branch or a full-fledged ticket branch tracked in JIRA.

3. Make and commit your edits. Content is written in reStructuredText. Our [reStructuredText Style Guide](https://bproto.gitlab.io/restructuredtext/style.html) covers the syntax you'll need.

4. Push your development branch to GitLab and make a merge request. The merge request page will help you track the publishing and testing status of your branch. 

5. Once you're done, press the blue button on your merge request to merge to `master`. Your changes are automatically published to the main URL: https://bproto.gitlab.io. Don't worry about messing things up, GitLab branch protections will ensure that your edits build successfully, and that your branch is up-to-date with `master`.

### Installing and building these docs locally

Assuming you've cloned the docs (following the guide above):

1. Create a [Python virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) for this project using your tool of choice: [virtualenvwrapper](http://virtualenvwrapper.readthedocs.org/en/latest/) or [pyvenv](https://docs.python.org/3.5/library/venv.html) (for Python 3).

2. Install dependencies

   ```bash
   $ pip install -r requirements.txt
   ```

3. Compile the HTML by running

   ```bash
   $ sphinx-build -b html src _build
   ```

The built site is in the `_build/html` directory.

### Editing entirely on GitLab

If you're in a hurry, you don't need to worry about cloning the Developer Guide; you can do everything on GitLab.com. See [GitLab's documentation](https://docs.gitlab.com/ee/user/project/web_ide/) on editing files and creating branches entirely from GitLab.com.
